msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-02-15 01:23+0000\n"
"PO-Revision-Date: 2020-04-19 12:08+0000\n"
"Last-Translator: framail <pedro@blinktil.com.br>\n"
"Language-Team: Portuguese (Brazil) <https://weblate.framasoft.org/projects/"
"mobilizon/backend/pt_BR/>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.11.2\n"

#: lib/web/templates/email/password_reset.html.eex:48
#: lib/web/templates/email/password_reset.text.eex:12
#, elixir-format
msgid "If you didn't request this, please ignore this email. Your password won't change until you access the link below and create a new one."
msgstr ""
"Se você não fez essa solicitação, favor ignorar este email.  Sua senha não "
"será modificada até que você acesse o link abaixo e crie uma nova senha."

#: lib/service/export/feed.ex:169
#, elixir-format
msgid "Feed for %{email} on Mobilizon"
msgstr "Feed para %{email} sobre o  Mobilizon"

#: lib/web/templates/email/email.html.eex:155
#: lib/web/templates/email/email.text.eex:16
#, elixir-format
msgid "%{instance} is a Mobilizon server."
msgstr "%{instance} é um servidor Mobilizon."

#: lib/web/templates/email/report.html.eex:41
#, elixir-format
msgid "%{reporter_name} (%{reporter_username}) reported the following content."
msgstr "%{reporter_name} (%{reporter_username}) reportou o seguinte conteúdo."

#: lib/web/templates/email/report.html.eex:52
#, elixir-format
msgid "%{title} by %{creator}"
msgstr "%{title} por %{creator}"

#: lib/web/templates/email/registration_confirmation.html.eex:58
#, elixir-format
msgid "Activate my account"
msgstr "Ativar a minha conta"

#: lib/web/templates/email/email.html.eex:124
#: lib/web/templates/email/email.text.eex:14
#, elixir-format
msgid "Ask the community on Framacolibri"
msgstr "Perguntar à comunidade Framacolibri"

#: lib/web/templates/email/report.html.eex:66
#: lib/web/templates/email/report.text.eex:13
#, elixir-format
msgid "Comments"
msgstr "Comentários"

#: lib/web/templates/email/report.html.eex:50
#: lib/web/templates/email/report.text.eex:6
#, elixir-format
msgid "Event"
msgstr "Evento"

#: lib/web/templates/email/registration_confirmation.html.eex:45
#, elixir-format
msgid "If you didn't request this, please ignore this email."
msgstr "Se você não não solicitou isso, favor ignorar este email."

#: lib/web/email/user.ex:48
#, elixir-format
msgid "Instructions to reset your password on %{instance}"
msgstr "Instruções para reiniciar a senha de %{instance}"

#: lib/web/templates/email/email.html.eex:156
#, elixir-format
msgid "Learn more about Mobilizon."
msgstr "Aprenda mais sobre Mobilizon."

#: lib/web/templates/email/registration_confirmation.html.eex:13
#, elixir-format
msgid "Nearly here!"
msgstr "Você está quase lá!"

#: lib/web/templates/email/email.html.eex:121
#: lib/web/templates/email/email.text.eex:12
#, elixir-format
msgid "Need some help? Something not working properly?"
msgstr "Precisa de ajuda? Algo não está funcionando bem?"

#: lib/web/templates/email/report.html.eex:13
#, elixir-format
msgid "New report on %{instance}"
msgstr "Novo relato em %{instance}"

#: lib/web/templates/email/report.html.eex:84
#: lib/web/templates/email/report.text.eex:22
#, elixir-format
msgid "Reason"
msgstr "Razão"

#: lib/web/templates/email/password_reset.html.eex:61
#, elixir-format
msgid "Reset Password"
msgstr "Reinicializar a senha"

#: lib/web/templates/email/password_reset.html.eex:41
#, elixir-format
msgid "Resetting your password is easy. Just press the button below and follow the instructions. We'll have you up and running in no time."
msgstr ""
"Reinicializar a sua senha é fácil. Apenas aperte o botão abaixo e siga as "
"instruções. Tudo estará funcionando em breve."

#: lib/web/templates/email/password_reset.html.eex:13
#, elixir-format
msgid "Trouble signing in?"
msgstr "Problemas para conectar?"

#: lib/web/templates/email/report.html.eex:104
#, elixir-format
msgid "View the report"
msgstr "Veja o relato"

#: lib/web/templates/email/registration_confirmation.html.eex:38
#, elixir-format
msgid "You created an account on %{host} with this email address. You are one click away from activating it."
msgstr ""
"Você criou uma conta em %{host} com este endereço de email. Falta um clique "
"pra você ativar sua conta."

#: lib/web/email/user.ex:28
#, elixir-format
msgid "Instructions to confirm your Mobilizon account on %{instance}"
msgstr "Instruções para confirmar sua conta Mobilizon na instância %{instance}"

#: lib/web/email/admin.ex:23
#, elixir-format
msgid "New report on Mobilizon instance %{instance}"
msgstr "Novo relato na instância Mobilizon %{instance}"

#: lib/web/templates/email/registration_confirmation.text.eex:1
#, elixir-format
msgid "Activate your account"
msgstr "Ativar sua conta"

#: lib/web/templates/email/event_participation_approved.html.eex:13
#, elixir-format
msgid "All good!"
msgstr "Tudo correto!"

#: lib/web/templates/email/event_participation_approved.html.eex:45
#: lib/web/templates/email/event_participation_approved.text.eex:7
#, elixir-format
msgid "An organizer just approved your participation. You're now going to this event!"
msgstr ""
"Um organizador acaba de aprovar a sua participação. Você estará indo neste "
"evento!"

#: lib/web/templates/email/event_participation_approved.html.eex:58
#: lib/web/templates/email/event_updated.html.eex:101
#, elixir-format
msgid "Go to event page"
msgstr "Va para a página do evento"

#: lib/web/templates/email/anonymous_participation_confirmation.html.eex:70
#: lib/web/templates/email/event_participation_approved.html.eex:70 lib/web/templates/email/event_updated.html.eex:113
#: lib/web/templates/email/event_updated.text.eex:21
#, elixir-format
msgid "If you need to cancel your participation, just access the event page through link above and click on the participation button."
msgstr ""
"Se você precisar cancelar a sua participação apenas acesse a página do "
"evento através do link acima e clique no botão participação."

#: lib/web/templates/email/anonymous_participation_confirmation.text.eex:11
#: lib/web/templates/email/event_participation_approved.text.eex:11
#, elixir-format
msgid "If you need to cancel your participation, just access the previous link and click on the participation button."
msgstr ""
"Se você precisa cancela ra sua participação, apenas acesso o link anterior e "
"clique no botão de participação."

#: lib/web/templates/email/email.text.eex:16
#, elixir-format
msgid "Learn more about Mobilizon:"
msgstr "Saiba mais sobre Mobilizon:"

#: lib/web/templates/email/report.text.eex:1
#, elixir-format
msgid "New report from %{reporter} on %{instance}"
msgstr "Novo relato sobre  %{reporter} na  %{instance}"

#: lib/web/templates/email/event_participation_approved.text.eex:1
#, elixir-format
msgid "Participation approved"
msgstr "Participação aprovada"

#: lib/web/templates/email/event_participation_rejected.text.eex:1
#, elixir-format
msgid "Participation rejected"
msgstr "Participação rejeitada"

#: lib/web/templates/email/password_reset.text.eex:1
#, elixir-format
msgid "Password reset"
msgstr "Senha redefinida"

#: lib/web/templates/email/password_reset.text.eex:7
#, elixir-format
msgid "Resetting your password is easy. Just click the link below and follow the instructions. We'll have you up and running in no time."
msgstr ""
"Redefinir sua senha é fácil. Clique no link abaixo e siga as instruções. "
"Estará pronto em pouco tempo."

#: lib/web/templates/email/event_participation_rejected.html.eex:13
#, elixir-format
msgid "Sorry!"
msgstr "Sinto muito !"

#: lib/web/templates/email/event_participation_rejected.html.eex:45
#: lib/web/templates/email/event_participation_rejected.text.eex:7
#, elixir-format
msgid "Unfortunately, the organizers rejected your participation."
msgstr "Infelizmente, a organização rejeitou a sua participação."

#: lib/web/templates/email/registration_confirmation.text.eex:5
#, elixir-format
msgid "You created an account on %{host} with this email address. You are one click away from activating it. If this wasn't you, please ignore this email."
msgstr ""
"Você criou a sua conta com a hospedagem %{host} e com este endereço de e-"
"mail. Você está a um clique para ativar sua conta. Se não for você favor "
"ignorar este e-mail."

#: lib/web/templates/email/anonymous_participation_confirmation.html.eex:38
#: lib/web/templates/email/event_participation_approved.html.eex:38
#, elixir-format
msgid "You requested to participate in event %{title}"
msgstr "Você solicitou a participação no evento %{title}"

#: lib/web/templates/email/anonymous_participation_confirmation.text.eex:5
#: lib/web/templates/email/event_participation_approved.text.eex:5 lib/web/templates/email/event_participation_rejected.html.eex:38
#: lib/web/templates/email/event_participation_rejected.text.eex:5
#, elixir-format
msgid "You requested to participate in event %{title}."
msgstr "Você solicitou participar no evento %{title}."

#: lib/web/email/participation.ex:91
#, elixir-format
msgid "Your participation to event %{title} has been approved"
msgstr "A sua participação no evento %{title} foi aprovada"

#: lib/web/email/participation.ex:70
#, elixir-format
msgid "Your participation to event %{title} has been rejected"
msgstr "A sua participação no evento %{title} foi rejeitada"

#: lib/web/templates/email/event_updated.html.eex:82
#, elixir-format
msgid "Ending of event"
msgstr "Fim do evento"

#: lib/web/email/event.ex:35
#, elixir-format
msgid "Event %{title} has been updated"
msgstr "Evento %{title} foi atualizado"

#: lib/web/templates/email/event_updated.html.eex:13
#: lib/web/templates/email/event_updated.text.eex:1
#, elixir-format
msgid "Event updated!"
msgstr "Evento atualizado!"

#: lib/web/templates/email/event_updated.text.eex:16
#, elixir-format
msgid "New date and time for ending of event: %{ends_on}"
msgstr "Nova data e horário para término do evento: %{ends_on}"

#: lib/web/templates/email/event_updated.text.eex:12
#, elixir-format
msgid "New date and time for start of event: %{begins_on}"
msgstr "Nova data e horário para início do evento: %{begins_on}"

#: lib/web/templates/email/event_updated.text.eex:8
#, elixir-format
msgid "New title: %{title}"
msgstr "Novo Título: %{title}"

#: lib/web/templates/email/event_updated.html.eex:72
#, elixir-format
msgid "Start of event"
msgstr "Início do evento"

#: lib/web/templates/email/event_updated.text.eex:5
#, elixir-format
msgid "The event %{title} was just updated"
msgstr "O evento %{title} acaba de ser atualizado"

#: lib/web/templates/email/event_updated.html.eex:38
#, elixir-format
msgid "The event %{title} was updated"
msgstr "O evento %{title} foi atualizado"

#: lib/web/templates/email/event_updated.html.eex:62
#, elixir-format
msgid "Title"
msgstr "Título"

#: lib/web/templates/email/event_updated.text.eex:19
#, elixir-format
msgid "View the updated event on: %{link}"
msgstr "Veja o evento atualizado em: %{link}"

#: lib/web/templates/email/password_reset.html.eex:38
#: lib/web/templates/email/password_reset.text.eex:5
#, elixir-format
msgid "You requested a new password for your account on %{instance}."
msgstr "Você solicitou uma nova senha para sua conta em %{instance}."

#: lib/web/templates/email/email.html.eex:95
#, elixir-format
msgid "In the meantime, please consider that the software is not (yet) finished. More information %{a_start}on our blog%{a_end}."
msgstr ""
"Enquanto isso, favor considerar que o aplicativo não está (ainda) terminado. "
"Saiba mais %{a_start}no nosso blog%{a_end}."

#: lib/web/templates/email/email.html.eex:94
#, elixir-format
msgid "Mobilizon is under development, we will add new features to this site during regular updates, until the release of %{b_start}version 1 of the software in the first half of 2020%{b_end}."
msgstr ""
"Mobilizon está em desenvolvimento, Iremos adicionar novos recursos neste "
"site durante as atualizações regulares, até a lançamento da %{b_start}versão "
"1 do aplicativo no primeiro semestre de 2020%{b_end}."

#: lib/web/templates/email/email.html.eex:91
#: lib/web/templates/email/email.text.eex:6
#, elixir-format
msgid "This is a demonstration site to test the beta version of Mobilizon."
msgstr "Este é um site de demonstração para testar a versão beta do Mobilizon."

#: lib/web/templates/email/email.html.eex:89
#, elixir-format
msgid "Warning"
msgstr "Atenção"

#: lib/web/templates/email/event_updated.html.eex:54
#, elixir-format
msgid "Event has been cancelled"
msgstr "Evento foi cancelado"

#: lib/web/templates/email/event_updated.html.eex:50
#, elixir-format
msgid "Event has been confirmed"
msgstr "Evento foi confirmado"

#: lib/web/templates/email/event_updated.html.eex:52
#, elixir-format
msgid "Event status has been set as tentative"
msgstr "O status do evento foi definido como tentativa"

#: lib/web/templates/email/email.html.eex:92
#, elixir-format
msgid "%{b_start}Please do not use it in any real way%{b_end}"
msgstr "%{b_start}Por favor não use este serviço para nenhum caso real%{b_end}"

#: lib/web/templates/email/report.html.eex:39
#, elixir-format
msgid "Someone on %{instance} reported the following content."
msgstr "Alguém da instância %{instance} reportou o seguinte conteúdo."

#: lib/web/templates/email/email.text.eex:10
#, elixir-format
msgid "In the meantime, please consider that the software is not (yet) finished. More information on our blog:"
msgstr ""
"Enquanto isso, favor considerar que o aplicativo não está (ainda) terminado. "
"Mais informação no nosso blog:"

#: lib/web/templates/email/email.text.eex:9
#, elixir-format
msgid "Mobilizon is under development, we will add new features to this site during regular updates, until the release of version 1 of the software in the first half of 2020."
msgstr ""
"Mobilizon está em desenvolvimento, iremos adicionar novos recursos neste "
"site durante atualizações regulares, até o lançamento da versão 1 do "
"aplicativo no primeiro semestre de 2020."

#: lib/web/templates/email/email.text.eex:7
#, elixir-format
msgid "Please do not use it in any real way"
msgstr "Por favor não utilize este serviço em nenhum caso real"

#: lib/web/templates/email/anonymous_participation_confirmation.html.eex:58
#, elixir-format
msgid "Confirm my participation"
msgstr "Confirmar minha participação"

#: lib/web/email/participation.ex:113
#, elixir-format
msgid "Confirm your participation to event %{title}"
msgstr "Confirmar sua participação no evento %{title}"

#: lib/web/templates/email/anonymous_participation_confirmation.html.eex:45
#: lib/web/templates/email/anonymous_participation_confirmation.text.eex:7
#, elixir-format
msgid "If you didn't request this email, you can simply ignore it."
msgstr "Se você não solicitou este email, você pode simplesmente ignorá-lo."

#: lib/web/templates/email/anonymous_participation_confirmation.html.eex:13
#: lib/web/templates/email/anonymous_participation_confirmation.text.eex:1
#, elixir-format
msgid "Participation confirmation"
msgstr "Confirmação de participação"

#: lib/web/templates/api/terms.html.eex:108
#, elixir-format
msgctxt "terms"
msgid "An internal ID for your current selected identity"
msgstr "Um ID interno para a sua atual identidade selecionada"

#: lib/web/templates/api/terms.html.eex:107
#, elixir-format
msgctxt "terms"
msgid "An internal user ID"
msgstr "Um ID interno"

#: lib/web/templates/api/terms.html.eex:45
#, elixir-format
msgctxt "terms"
msgid "Any of the information we collect from you may be used in the following ways:"
msgstr ""
"Qualquer informação que coletamos de você poderá ser utilizada das seguintes "
"maneiras:"

#: lib/web/templates/api/terms.html.eex:4
#, elixir-format
msgctxt "terms"
msgid "Basic account information"
msgstr "Informações básicas da conta"

#: lib/web/templates/api/terms.html.eex:28
#, elixir-format
msgctxt "terms"
msgid "Do not share any dangerous information over Mobilizon."
msgstr "Não compartilhe nenhuma informação perigosa no Mobilizon."

#: lib/web/templates/api/terms.html.eex:126
#, elixir-format
msgctxt "terms"
msgid "Do we disclose any information to outside parties?"
msgstr "Nós divulgamos alguma informação a terceiros?"

#: lib/web/templates/api/terms.html.eex:101
#, elixir-format
msgctxt "terms"
msgid "Do we use cookies?"
msgstr "Utilizamos cookies?"

#: lib/web/templates/api/terms.html.eex:59
#, elixir-format
msgctxt "terms"
msgid "How do we protect your information?"
msgstr "Como nós protegemos as suas informações?"

#: lib/web/templates/api/terms.html.eex:32
#, elixir-format
msgctxt "terms"
msgid "IPs and other metadata"
msgstr "IPs e outros metadados"

#: lib/web/templates/api/terms.html.eex:111
#, elixir-format
msgctxt "terms"
msgid "If you delete these informations, you need to login again."
msgstr "Se você apagar esta informação, você precisa entrar novamente."

#: lib/web/templates/api/terms.html.eex:113
#, elixir-format
msgctxt "terms"
msgid "If you're not connected, we don't store any information on your device, unless you participate in an event\n  anonymously. In that case we store the hash of the UUID and participation status in your browser so that we may\n  display participation status. Deleting these informations will only stop displaying participation status in your\n  browser."
msgstr ""
"Se você não estiver conectado, nós não armazenamos nenhuma informação no seu "
"aparelho, a menos que você participe de um evento\n"
"anonimamente. Neste caso nos armazenamos o hash do UUID e o status da "
"participação no seu navegador, de modo que nós podemos\n"
"exibir o status da participação. Apagando esta informação vai somente "
"impedir de exibir o status da participação no seu navegador."

#: lib/web/templates/api/terms.html.eex:123
#, elixir-format
msgctxt "terms"
msgid "Note: These informations are stored in your localStorage and not your cookies."
msgstr ""
"Nota: Estas informações são armazenadas no seu armazenamento local "
"(localStorage) e não no seus cookies."

#: lib/web/templates/api/terms.html.eex:18
#, elixir-format
msgctxt "terms"
msgid "Published events and comments"
msgstr "Eventos publicados e comentários"

#: lib/web/templates/api/terms.html.eex:83
#, elixir-format
msgctxt "terms"
msgid "Retain the IP addresses associated with registered users no more than 12 months."
msgstr ""
"Conserva o endereço IP associado com os usuários registrados não mais que 12 "
"meses."

#: lib/web/templates/api/terms.html.eex:109
#, elixir-format
msgctxt "terms"
msgid "Tokens to authenticate you"
msgstr "Tokens para autenticar você"

#: lib/web/templates/api/terms.html.eex:35
#, elixir-format
msgctxt "terms"
msgid "We also may retain server logs which include the IP address of every request to our server."
msgstr ""
"Também podemos conservar os registros dos servidores que incluem o endereço "
"IP de cada solicitação ao nosso servidor."

#: lib/web/templates/api/terms.html.eex:5
#, elixir-format
msgctxt "terms"
msgid "We collect information from you when you register on this server and gather data when you participate in the\n      platform by reading, writing, and interacting with content shared here. If you register on this server, you will\n      be asked to enter an e-mail address, a password and at least an username. Your e-mail address will be verified by\n      an email containing a unique link. If that link is visited, we know that you control the e-mail address. You may\n      also enter additional profile information such as a display name and biography, and upload a profile picture and\n      header image. The username, display name, biography, profile picture and header image are always listed publicly.\n      You may, however, visit this server without registering."
msgstr ""
"Nós coletamos informações de você quando você se registra neste servidor e "
"reunimos dados quando você participa na plataforma\n"
"      seja lendo, escrevendo e interagindo com o conteúdo compartilhado "
"aqui. Se você se registra neste servidor, será solicitado que \n"
"      você insira o seu endereço de email, uma senha e pelo ao menos um nome "
"de usuário. O seu endereço de email será verificado\n"
"      através de um email envaido a você e contendo um link. Se esse link é "
"visitado, nós saberemos que você controla esse endereço\n"
"      de email. Você talvez também insira informações adicionais no seu "
"perfil, tais como o nome a exibir e sua biografia e ainda suba \n"
"     uma foto sua para o perfil e uma image de cabeçalho. O nome de usuário, "
"nome a exibir, biografia, foto do perfil e imagens de \n"
"      cabeçalho são sempre exibidas publicamente. Mas você pode, no entanto, "
"visitar este site sem se registrar."

#: lib/web/templates/api/terms.html.eex:130
#, elixir-format
msgctxt "terms"
msgid "We do not sell, trade, or otherwise transfer to outside parties your personally identifiable information. This\n  does not include trusted third parties who assist us in operating our site, conducting our business, or servicing\n  you, so long as those parties agree to keep this information confidential. We may also release your information\n  when we believe release is appropriate to comply with the law, enforce our site policies, or protect ours or\n  others rights, property, or safety."
msgstr ""
"Nós não vendemos, negociamos, ou então transferimos para terceiros suas "
"informações de identificação pessoal.\n"
"  Isso não inclui parceiros, nos quais confiamos, que nos dão assistência na "
"operação do nosso site, na condução do\n"
"  nosso negócio, ou servido você, contanto que esses parceiros concordem em "
"manter essas informações como confidencial.\n"
"  Nós podemos também liberar sua informação quando nós acreditamos que a "
"liberação seja apropriada para o cumprimento\n"
"  da lei, fazer cumprir a política de nosso site ou proteger o direito, a "
"propriedade ou segurança nossa e de outros."

#: lib/web/templates/api/terms.html.eex:62
#, elixir-format
msgctxt "terms"
msgid "We implement a variety of security measures to maintain the safety of your personal information when you enter,\n  submit, or access your personal information. Among other things, your browser session, as well as the traffic between\n  your applications and the API, are secured with SSL/TLS, and your password is hashed using a strong one-way\n  algorithm."
msgstr ""
"Implementamos uma variedade de medidas de segurança para manter protegida a "
"sua informação pessoal quando você\n"
"  as envia ou quando você acessa sua informação pessoal. Entre outras "
"coisas, como  a sessão do seu navegador, e também\n"
"  o tráfico entre a suas aplicações e o API estão ambos protegidos com SSL/"
"TLS, e a sua senha esta em forma de hash usando\n"
"  um forte algoritmo de mão única."

#: lib/web/templates/api/terms.html.eex:103
#, elixir-format
msgctxt "terms"
msgid "We store the following information on your device when you connect:"
msgstr ""
"Nós armazenamos a seguinte informação no seu aparelho quando você conecta:"

#: lib/web/templates/api/terms.html.eex:72
#, elixir-format
msgctxt "terms"
msgid "We will make a good faith effort to:"
msgstr "Iremos fazer um esforço de boa fé para:"

#: lib/web/templates/api/terms.html.eex:43
#, elixir-format
msgctxt "terms"
msgid "What do we use your information for?"
msgstr "Para que usamos a sua informação?"

#: lib/web/templates/api/terms.html.eex:71
#, elixir-format
msgctxt "terms"
msgid "What is our data retention policy?"
msgstr "Qual é a nossa política de retenção de dados?"

#: lib/web/templates/api/terms.html.eex:92
#, elixir-format
msgctxt "terms"
msgid "You can request and download an archive of your content, including your posts, media attachments, profile picture,\n  and header image."
msgstr ""
"Você pode solicitar e baixar um arquivo do seu conteúdo, incluindo suas "
"publicações, anexos de médias, foto do perfil\n"
"  e imagem do cabeçalho."

#: lib/web/templates/api/terms.html.eex:100
#, elixir-format
msgctxt "terms"
msgid "You may irreversibly delete your account at any time."
msgstr "Você poderá excluir sua conta a qualquer momento."

#: lib/web/templates/api/terms.html.eex:142
#, elixir-format
msgctxt "terms"
msgid "Your content may be downloaded by other servers in the network. Your content is delivered to the servers\n  following your instance, and direct messages are delivered to the servers of the recipients, in so far as these\n  recipients reside on a different server than this one."
msgstr ""
"Seu conteúdo poderá ser baixado por outros servidores na rede. Seu conteúdo "
"é entregue para outros servidores\n"
"  seguindo sua instância e as mensagens diretas são entregues para os "
"servidores dos destinatários, seja qual for\n"
"  a distância em que os recipientes dos destinatários estejam ou residem em "
"um servidor diferente desse."

#: lib/web/templates/api/terms.html.eex:20
#, elixir-format
msgctxt "terms"
msgid "Your events and comments are delivered to other instances that follow your own, meaning they are delivered to\n      different servers and copies are stored there. When you delete events or comments, this is likewise delivered to\n      these other instances. The action of joining an event is federated as well. Please keep in mind that the operators\n      of the server and any receiving server may view such messages, and that recipients may screenshot, copy or\n      otherwise re-share them."
msgstr ""
"Seus eventos e comentários são entregues em outras instâncias que seguem a "
"sua própria, isto é, eles são entregues\n"
"      a diferentes servidores e cópias são armazenadas lá. Quando você apaga "
"eventos ou comentários, esta instrução\n"
"      é do mesmo modo entregue a essas instâncias. A ação de se juntar a um "
"evento é federada também. Favor manter\n"
"      em mente que os operadores dos servidores e qualquer servidor possam "
"ver essas mensagens e fazerem uma captura\n"
"      e, então, compartilha-las novamente."

#: lib/web/templates/api/terms.html.eex:159
#, elixir-format
msgctxt "terms"
msgid "Changes to our Privacy Policy"
msgstr "Mudanças em nossa Política de Privacidade"

#: lib/web/templates/api/terms.html.eex:154
#, elixir-format
msgctxt "terms"
msgid "If this server is in the EU or the EEA: Our site, products and services are all directed to people who are at least 16 years old. If you are under the age of 16, per the requirements of the GDPR (<a href=\"https://en.wikipedia.org/wiki/General_Data_Protection_Regulation\">General Data Protection Regulation</a>) do not use this site."
msgstr ""
"Se este servidor estiver na Comunidade Européia (EU) Área Econômica Européia "
"(EEA); os produtos e serviços de nosso site são todos dirigidos para pessoas "
"que tenham pelo ao menos 16 anos. Se você estiver abaixo dessa idade, , de "
"acordo com os requisitos da GDPR (<a href=\"https://en.wikipedia.org/wiki/"
"General_Data_Protection_Regulation\">General Data Protection Regulation / "
"Regulamentação de Proteção de Dados Gerais- </a>) não use este site."

#: lib/web/templates/api/terms.html.eex:155
#, elixir-format
msgctxt "terms"
msgid "If this server is in the USA: Our site, products and services are all directed to people who are at least 13 years old. If you are under the age of 13, per the requirements of COPPA (<a href=\"https://en.wikipedia.org/wiki/Children%27s_Online_Privacy_Protection_Act\">Children's Online Privacy Protection Act</a>) do not use this site."
msgstr ""
"Se este servidor estiver nos EUA: os produtos e serviços do nosso site são "
"todos direcionados a pessoas que tenham pelo ao menos 13 anos de idade. Se "
"você estiver abaixo desta idade, de acordo com o COPPA (<a href=\""
"https://en.wikipedia.org/wiki/Children%27s_Online_Privacy_Protection_Act\">"
"Children's Online Privacy Protection Act / Ato de Proteção da Privacidade "
"Online das Crianças </a>) não use este site."

#: lib/web/templates/api/terms.html.eex:161
#, elixir-format
msgctxt "terms"
msgid "If we decide to change our privacy policy, we will post those changes on this page."
msgstr ""
"Se decidirmos mudar nossa política de privacidade, iremos publicar essas "
"mudanças nesta página."

#: lib/web/templates/api/terms.html.eex:156
#, elixir-format
msgctxt "terms"
msgid "Law requirements can be different if this server is in another jurisdiction."
msgstr ""
"Os requisitos legais podem ser diferentes se este servidor estiver em outra "
"jurisdição."

#: lib/web/templates/api/terms.html.eex:163
#, elixir-format
msgctxt "terms"
msgid "Originally adapted from the <a href=\"https://mastodon.social/terms\">Mastodon</a> and <a href=\"https://github.com/discourse/discourse\">Discourse</a> privacy policies."
msgstr ""
"Originalmente adaptado das politicas privadas de <a href=\"https://mastodon."
"social/terms\">Mastodon</a> e <a href=\"https://github.com/discourse/"
"discourse\">Discourse</a>."

#: lib/web/templates/api/terms.html.eex:75
#, elixir-format
msgctxt "terms"
msgid "Retain server logs containing the IP address of all requests to this server, in so far as such logs are kept, no more\n        than 90 days."
msgstr ""
"Conserve os registros do servidor que contenham o endereço IP de todas as "
"solicitações a este servidor, de modo que permaneçam não mais que\n"
"        90 dias."

#: lib/web/templates/api/terms.html.eex:152
#, elixir-format
msgctxt "terms"
msgid "Site usage by children"
msgstr "Uso do site por crianças"

#: lib/web/templates/api/terms.html.eex:55
#, elixir-format
msgctxt "terms"
msgid "The email address you provide may be used to send you information, updates and notifications about other people\n    interacting with your content or sending you messages and to respond to inquiries, and/or other requests or\n    questions."
msgstr ""
"O endereço email que você fornecer pode ser usado para enviar informações, "
"atualizações e notificações de outras pessoas\n"
"    que interajam com o seu conteúdo ou para enviar à você mensagens e para "
"responder a solicitações, e/ou outros pedidos\n"
"    ou dúvidas."

#: lib/web/templates/api/terms.html.eex:162
#, elixir-format
msgctxt "terms"
msgid "This document is CC-BY-SA. It was last updated January 16, 2020."
msgstr ""
"Este documento tem licença CC-BY-SA. e foi atualizada em 16 de Janeiro de "
"2020."

#: lib/web/templates/api/terms.html.eex:53
#, elixir-format
msgctxt "terms"
msgid "To aid moderation of the community, for example comparing your IP address with other known ones to determine ban\n    evasion or other violations."
msgstr ""
"Para ajudar na moderação da comunidade, por exemplo, compare seu endereço IP "
"com outros para saber conhecidas para determinar a proibição,\n"
"    evasões ou outras violações."

#: lib/web/templates/api/terms.html.eex:51
#, elixir-format
msgctxt "terms"
msgid "To provide the core functionality of Mobilizon. Depending on this instance's policy you may only be able to\n    interact with other people's content and post your own content if you are logged in."
msgstr ""

#: lib/web/templates/api/terms.html.eex:1
#, elixir-format
msgctxt "terms"
msgid "What information do we collect?"
msgstr ""

#: lib/web/templates/email/email_changed_new.html.eex:38
#: lib/web/templates/email/email_changed_new.text.eex:5
#, elixir-format
msgid "Confirm the new address to change your email."
msgstr ""

#: lib/web/templates/email/email_changed_new.html.eex:64
#: lib/web/templates/email/email_changed_new.text.eex:10
#, elixir-format
msgid "If this change wasn't initiated by you, please ignore this email. The email address for the Mobilizon account won't change until you access the link above."
msgstr ""

#: lib/web/templates/email/email_changed_old.html.eex:62
#: lib/web/templates/email/email_changed_old.text.eex:9
#, elixir-format
msgid "If you did not ask to change your email, it is likely that someone has gained access to your account. Please change your password immediately or contact the server admin if you're locked out of your account."
msgstr ""

#: lib/web/email/user.ex:175
#, elixir-format
msgid "Mobilizon on %{instance}: confirm your email address"
msgstr ""

#: lib/web/email/user.ex:152
#, elixir-format
msgid "Mobilizon on %{instance}: email changed"
msgstr ""

#: lib/web/templates/email/email_changed_old.html.eex:13
#: lib/web/templates/email/email_changed_old.text.eex:1
#, elixir-format
msgid "New email address"
msgstr ""

#: lib/web/templates/email/email_changed_old.html.eex:38
#: lib/web/templates/email/email_changed_old.text.eex:5
#, elixir-format
msgid "The email address for your account on %{host} is being changed to:"
msgstr ""

#: lib/web/templates/email/email_changed_new.html.eex:13
#: lib/web/templates/email/email_changed_new.html.eex:51 lib/web/templates/email/email_changed_new.text.eex:1
#, elixir-format
msgid "Verify email address"
msgstr ""
